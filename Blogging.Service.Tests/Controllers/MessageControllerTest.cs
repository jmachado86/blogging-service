﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Blogging.BLL.Implementation;
using Blogging.BLL.Interfaces;
using Blogging.BLL.Models;
using Blogging.WebApi.Controllers;

namespace Blogging.Service.Tests.Controllers
{
    [TestClass]
    public class MessageControllerTest
    {
        private MessageController controller;
        private IMessageService messageService;

        [TestInitialize]
        public void Setup()
        {
            messageService = new MessageService();
            controller = new MessageController();
        }

        [TestMethod]
        public void Should_Be_Able_To_See_ALl_Message()
        {
            var result = controller.Get();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Should_Be_Able_To_Post_A_Message()
        {
            var result = controller.Post(new Message() { Content = "Test"});
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.TimeStamp);
            Assert.IsTrue(result.Id > 0);
        }

        [TestMethod]
        public void Should_Be_Able_To_Delete_A_Mesage()
        {
            var message = messageService.SaveMessage(new Message() {  Content = "Test"});
            controller.Delete(message.Id);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Should_Be_Return_An_Error_Message_When_Message_Does_Not_Exits()
        {
            controller.Delete(1);
        }
    }
}
