﻿using Blogging.BLL.Implementation;
using Blogging.BLL.Interfaces;
using Blogging.BLL.Models;
using Blogging.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Blogging.Service.Controllers
{
    public class MessageController : ApiController
    {
        private readonly IMessageService messageService;

        public MessageController(IMessageService messageService)
        {
            this.messageService = messageService;        
        }

        public IEnumerable<Message> Get()
        {
            return messageService.GetMessages();
        }

        public Message Post([FromBody] Message message)
        {
            return messageService.SaveMessage(message);
        }

        public void Delete(int id)
        {
            messageService.DeleteMessage(id);
        }

    }
}
