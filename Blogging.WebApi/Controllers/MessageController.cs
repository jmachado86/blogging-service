﻿using Blogging.BLL.Implementation;
using Blogging.BLL.Interfaces;
using Blogging.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Blogging.WebApi.Controllers
{
    public class MessageController : ApiController
    {
        private readonly IMessageService messageService;

        public MessageController()
        {
            this.messageService = new MessageService();
        }

        /// <summary>
        /// Returns all messages
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Message> Get()
        {
            return messageService.GetMessages();
        }

        /// <summary>
        /// Saves a message
        /// </summary>
        /// <param name="message">The Message to be saved</param>
        /// <returns></returns>
        public Message Post([FromBody] Message message)
        {
            return messageService.SaveMessage(message);
        }

        /// <summary>
        /// Deletes a message
        /// </summary>
        /// <param name="id">Id of the message to be deleted</param>
        public void Delete(int id)
        {
            messageService.DeleteMessage(id);
        }
    }
}
