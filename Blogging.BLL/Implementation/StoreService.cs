﻿using Blogging.BLL.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blogging.BLL.Implementation
{
    public class StoreService : IStoreService, IDisposable
    {
        private readonly IDbConnection connection;
        private readonly string connectionString;

        public StoreService()
        {

        }

        public StoreService(string connectionString)
        {
            this.connectionString = connectionString;
            this.connection = new SqlConnection(connectionString);
            this.connection.Open();
        }

        public virtual void Save<T>(T t) where T : class
        {
            connection.Insert(t);
        }

        public virtual void Save<T>(List<T> t)
        {
            connection.Insert(t);
        }

        public virtual T QueryScalar<T>(string command, object param = null)
        {
            return connection.ExecuteScalar<T>(command, param);
        }

        public virtual IEnumerable<T> QueryScalar<T>(string command, Dictionary<string, object> param)
        {
            return connection.Query<T>(command, new DynamicParameters(param));
        }

        public virtual Task<T> QueryScalarAsync<T>(string command, object param = null)
        {
            return connection.ExecuteScalarAsync<T>(command, param);
        }

        public virtual dynamic QueryObject(string command, object param = null)
        {
            return connection.QuerySingle(command, param);
        }

        public virtual IEnumerable<T> Query<T>(string command, object param = null)
        {
            return connection.Query<T>(command, param);
        }

        public virtual IEnumerable<TE> QueryList<TE>(string command, object param = null)
        {
            return connection.Query<TE>(command, param);
        }

        public virtual IEnumerable<T> GetAll<T>() where T : class
        {
            return connection.GetAll<T>();
        }

        public virtual T QueryFirst<T>(string command, object param = null)
        {
            return connection.QuerySingle<T>(command, param);
        }

        public virtual void Execute(string command, object param = null)
        {
            connection.Execute(command, param);
        }

        public virtual void ExecuteSproc(string command, object param = null)
        {
            connection.Execute(command, param, commandType: CommandType.StoredProcedure);
        }

        public virtual void Delete<T>(T t) where T : class
        {
            connection.Delete<T>(t);
        }

        public void Update<T>(T t) where T : class
        {
            connection.Update<T>(t);
        }

        public virtual void Dispose()
        {
            connection.Close();
            connection.Dispose();
        }
    }
}
