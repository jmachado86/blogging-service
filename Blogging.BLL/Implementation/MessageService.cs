﻿using Blogging.BLL.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using Blogging.BLL.Models;
using System.Configuration;

namespace Blogging.BLL.Implementation
{
    public class MessageService : IMessageService
    {
        private readonly IStoreService storeService;

        public MessageService()
        {
            this.storeService = new StoreService(ConfigurationManager.ConnectionStrings["Blogging"].ConnectionString);
        }

        public void DeleteMessage(int id)
        {
            var message = storeService.QueryFirst<Message>("select * from Message where Id = @id", new { id = id});
            if (message == null)
                throw new Exception("Message not found");
            storeService.Delete<Message>(message);
        }

        public IList<Message> GetMessages()
        {
            return storeService.GetAll<Message>().ToList();
        }

        public Message SaveMessage(Message message)
        {
            message.TimeStamp = DateTime.Now;
            storeService.Save<Message>(message);
            return message;
        }
    }
}
