﻿using Blogging.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blogging.BLL.Interfaces
{
    public interface IMessageService
    {
        IList<Message> GetMessages();

        Message SaveMessage(Message message);

        void DeleteMessage(int id);
    }
}
