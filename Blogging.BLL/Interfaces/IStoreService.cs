﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blogging.BLL.Interfaces
{
    public interface IStoreService
    {
        void Save<T>(T t) where T : class;
        void Save<T>(List<T> t);
        T QueryScalar<T>(string command, object param = null);
        Task<T> QueryScalarAsync<T>(string command, object param = null);
        T QueryFirst<T>(string command, object param = null);
        IEnumerable<T> Query<T>(string command, object param = null);
        IEnumerable<T> GetAll<T>() where T : class;
        IEnumerable<T> QueryList<T>(string command, object param = null);
        dynamic QueryObject(string command, object param = null);
        void Execute(string command, object param = null);
        void Dispose();
        void Delete<T>(T t) where T : class;
        void Update<T>(T t) where T : class;

        IEnumerable<T> QueryScalar<T>(string command, Dictionary<string, object> param);
        void ExecuteSproc(string command, object param = null);
    }
}
